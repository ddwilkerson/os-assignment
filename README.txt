My first OS program.

In order to build and run this program all you need to do is run the build
script.
./build.sh

The user and password are:
U: dakota
P: password
It is case sensitive.

The output of the program is a welcome message and a user prompt. Once a 
username is provided a password prompt will appear. After a password is 
provided, the screen and text will change color and print "Hello world!" and 
the first twenty prime numbers.

A .bin file is included for convenience. 
