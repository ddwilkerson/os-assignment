[BITS 16]
org 0x7C00
_start:					;;START OF PROGRAM;;
	call _cls			;clear screen
	cld					;clear direction bit
	mov ebx, esi		;store the original addr of esi
	mov edx, edi		;store the original addr of edi
	mov esi, first_os
	call _println

username_stage:
	mov esi, ebx
	mov esi, user_prompt		;moving user prompt into source register
	call _print

user_setup:
	mov esi, ebx		;restore esi addr
	mov esi, user		;move static username into esi
	xor ecx, ecx		;set counter to zero

user_input:				;username input is handled here
	mov ah, 0h
	int 16h				;read in interrupt, result in al
	stosb				;store copy of result of interrupt in edi
	cmp al, CR			;compare al to 'enter'
	je	ui_done			;user finished inputting username
	mov ah, 0Eh			;print out
	int 10h				;interrupt, display al
	inc ecx				;increment counter
	jmp user_input		;loop input

ui_done:				;user input is complete
	cmp ecx, user_len	;compare number of typed letters to username length
	jg try_again		;if typed letters too long try again
	mov ah, 0Eh
	mov al, CR			;enter down
	int 10h
	mov al, LF			;reset curser
	int 10h
	mov edi, edx		;restore original addr of edi
	mov esi, ebx		;restore original addr of esi
	mov esi, user		;mov static user into esi for compare
	mov ecx, user_len	;setting up counter

user_check:				;check if user name is correct
	mov al, [edi]		;al contains what was typed
	mov ah, [esi]		;move compare string into ah
	cmp al, ah			;compare al and ah
	jne try_again		;if they dont match jump to end and try again
	dec ecx				;decrement counter
	jz password_stage	;if counter = 0 then move on
	inc esi				;inc source addr
	inc edi				;inc destination addr
	jmp user_check		;loop

password_stage:
	mov edi, edx
	mov esi, ebx
	mov esi, pass_prompt		;moving password prompt into source register
	call _print

pass_setup:
	mov esi, ebx
	mov esi, pass		;move static pass into esi
	xor ecx, ecx		;set counter to zero

pass_input:				;where input for password is handled
	mov ah, 0h
	int 16h				;read in interrupt, result in al
	stosb				;store copy of result of interrupt in edi
	cmp al, CR			;compare al to 'enter'
	je	pi_done			;user finished inputting password
	mov al, 2Ah
	mov ah, 0Eh			;print out
	int 10h				;interrupt, display al
	inc ecx				;increment counter
	jmp pass_input		;loop input

pi_done:				;password input complete
	cmp ecx, pass_len	;compare number of typed letters to password length
	jg try_again		;if typed letters too long try again
	mov ah, 0Eh
	mov al, CR			;enter down
	int 10h
	mov al, LF			;reset curser
	int 10h
	mov edi, edx		;restore previous addr
	mov esi, ebx
	mov esi, pass		;mov static user into esi for compare
	mov ecx, pass_len	;setting up counter

pass_check:				;check if user name is correct
	mov al, [edi]		;al contains what was typed
	mov ah, [esi]		;move compare string into ah
	cmp al, ah			;compare al and ah
	jne try_again		;if they dont match jump to end and try again
	dec ecx				;decrement counter
	jz done				;if counter = 0 then move on
	inc esi				;inc source addr
	inc edi				;inc destination addr
	jmp pass_check		;loop

done:					;display color changed hello world and first 20 primes
	call _cls
	mov ah, 06h    ; Scroll up function
	xor al, al     ; Clear entire screen
	xor cx, cx     ; Upper left corner CH=row, CL=column
	mov dx, 184Fh  ; lower right corner DH=row, DL=column
	mov bh, 1Eh    ; YellowOnBlue
	int 10h
	mov esi, ebx
	mov esi, hello		;move msg into register
	call _println		;output msg
	;mov esi, ebx
	;mov esi, prime		;move msg into register
	;call _println		;output msg
	jmp halt

try_again:				;used for when the user inputs the wrong info
						;sends the user back to top of program with an error msg
	call _cls				;clear screen
	mov edi, edx
	mov esi, ebx
	mov esi, error			;move error msg into source
	call _println			;print error msg
	jmp username_stage		;jump back to top

halt:
	cli
	hlt

;------------------------- methods --------------------------------;

;------clear screen------;
_cls:
	mov ax, 0h
	int 10h
ret

;------print method------;
;;assumes esi is pointing to the beginning of the string
;;prints out the string without a newline
_print:
print_loop:
	mov ah, 0Eh
	lodsb
	or al, al			;check if al contains nothing
	int 10h				;display char
	jnz print_loop
ret						;return

;------println method------;
;;assumes esi is pointing to the beginning of the string
;;prints out the string and a newline
_println:
ln_loop:
	mov ah, 0Eh
	lodsb
	or al, al			;check if al contains nothing
	int 10h				;display char
	jnz ln_loop
	mov al, CR			;formatting for asthetics
	int 10h
	mov al, LF
	int 10h
ret						;return

;----------------------- Data Seciton -------------------------------;
data:
	CR EQU 0Dh
	LF EQU 0Ah
	esi_start	dq 0
	edi_start	dq 0
	first_os	db "This is my first OS :)", 0
	user_prompt	db "User:", 0
	pass_prompt	db "Pass:", 0
	user		db "dakota", 0
	user_len	EQU $-user-1		;minus one because '$' inlcudes \0 in string
	pass		db "password", 0
	pass_len	EQU $-pass-1
	error	db "Incorrect, please try again...",0
	hello	db "Hello World!", CR, LF
	prime	db "2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71", 0
	debug	db "Debugging", 0

TIMES 510 - ($ - $$) db 0
DW 0xAA55
